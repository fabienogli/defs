# DEFS


To run the project, one must install [Docker](https://docs.docker.com/install/) and [Docker-Compose](https://docs.docker.com/compose/install/).
 

## Configuration 

Before running the project, some configuration must be done :

For each service, there should be a .env file with the different environment variable configurations.  
As a documentation, there is also a .env.example. For basic usage, you can just copy the .env.example :   
`$ cp loadbalancer/.env.example loadbalancer/.env`  
`$ cp storage/.env.example storage/.env`  
`$ cp supervisor/.env.example supervisor/.env`

If you have go installed (see the [documentation](https://golang.org/doc/) for more details), 
we made a script for just this ! You can run 
`go run utils/main.go`, which will copy the .env.example files.

If you change some variables, you have to make sure that the ports are coherent with the docker-compose.yml file (for exposing the correct ports)

## Running

To run the application, you can use docker-compose :   
`$ docker-compose up`

This will pull all the docker images and build the containers, then run them using the default docker-compose.yml configuration.

## Using

For now, no client is developed so the interraction with the application are made using the HTTP protocol. 
Pretty much any client will do but we recommend using cURL : 

For example, with cURL, you can upload a file to our service with   
`$ curl -X POST -F filename=hello.txt -F file=@hello.txt localhost:8080/file`

Which should result in a json like : 
```json
{
  "hash": "734cad14909bedfafb5b273b6b0eb01fbfa639587d217f78ce9639bba41f4415"
}
```

Then, you can download the file later with the following command :

`$ curl localhost:8080/file/734cad14909bedfafb5b273b6b0eb01fbfa639587d217f78ce9639bba41f4415`

